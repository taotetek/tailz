/*  =========================================================================
    tailz - description

    Copyright (c) the Contributors as noted in the AUTHORS file.       
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

/*
@header
    tailz - 
@discuss
@end
*/

#include "tailz_classes.h"

int main(int argc, char *argv [])
{
    int argn = 1;
    if (argn < argc && streq (argv [argn], "-h")) {
        puts ("syntax: tailz [-e ENDPOINTS] [-t TOPICS] [-s SERVERCERT] [-c CLIENTCERT]");
        puts (" -e = comma delimited list of zeromq endpoints");
        puts (" -t = comma delimited list of topics");
        puts (" -s = path to server curve certificate");
        puts (" -c = path to client curve certificate");
		puts (" -z = zeromq socket type");
        exit (0);
    }

    char *endpoints = NULL;
    char *topics = NULL;
    char *servercert = NULL;
    char *clientcert = NULL;
    int socktype;

    while (argn < argc && (*argv [argn] == '-' )) {
        if (streq (argv [argn], "-e")) {
            argn++;
            endpoints = argv [argn];
        }
        else
        if (streq (argv [argn], "-t")) {
            argn++;
            topics = argv [argn];
        }
        else
        if (streq (argv [argn], "-s")) {
            argn++;
            servercert = argv [argn];
        }
        else
        if (streq (argv [argn], "-c")) {
            argn++;
            clientcert = argv [argn];
        }
		else
        if (streq (argv [argn], "-z")) {
            argn++;
            if (streq (argv [argn], "ZMQ_SUB"))
                socktype = ZMQ_SUB;
            else if (streq (argv [argn], "ZMQ_DISH"))
                socktype = ZMQ_DISH;
        }
        else {
            puts ("Invalid option, run tailz -h to see options");
            exit (0);
        }
        argn++;
    }

    bool all = true;
    if (!endpoints) {
        puts ("-e <endpoints> is mandatory");
        all = false;
    }

    if (!topics) {
        puts ("-t <topics> is mandatory");
        all = false;
    }

    if (!servercert) {
        puts ("-s <servercert> is mandatory");
        all = false;
    }

    if (!clientcert) {
        puts ("-c <clientcert> is mandatory");
        all = false;
    }

    if (!all)
        exit (1);

    tailer_t *tailer = tailer_new (socktype, endpoints, topics, servercert, clientcert);
    int rc = tailer_connect (tailer);
    if (rc == -1) {
        puts ("tailer could not connect");
        exit (1);
    }

    bool loop = true;
    while (loop) {
        const char *line = tailer_next (tailer);
        printf ("%s\n", line);
    }

    tailer_destroy (&tailer);
}

