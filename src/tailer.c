/*  =========================================================================
    tailer - class description

    Copyright (c) the Contributors as noted in the AUTHORS file.       
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

/*
@header
    tailer - 
@discuss
@end
*/

#include "tailz_classes.h"

//  Structure of our class

struct _tailer_t {
    int socktype;
    zsock_t *sock;
    zlist_t *endpoints;
    zlist_t *topics;
    zcert_t *clientcert;
    zcert_t *servercert;
};


static void
s_parse_items (zlist_t *list, const char *items) {
    char item [256];
    while (*items) {
        const char *delimiter = strchr (items, ',');

        if (!delimiter)
            delimiter = items + strlen (items);

        assert(delimiter - items < 256);
        memcpy (item, items, delimiter - items);
        item [delimiter - items] = 0;
        zlist_append (list, item);

        if (*delimiter == 0)
            break;

        items = delimiter + 1;
    }
}


//  --------------------------------------------------------------------------
//  Create a new tailer

tailer_t *
tailer_new (int socktype, const char *endpoints, const char *topics, const char *servercert, const char *clientcert)
{
    tailer_t *self = (tailer_t *) zmalloc (sizeof (tailer_t));
    assert (self);
   
    self->socktype = socktype; 
    self->sock = zsock_new (self->socktype);
    self->topics = zlist_new ();
    zlist_autofree (self->topics);
    self->clientcert = zcert_load (clientcert);
    self->servercert = zcert_load (servercert);

    self->endpoints = zlist_new ();
    zlist_autofree (self->endpoints);
    s_parse_items (self->endpoints, endpoints);

    self->topics = zlist_new ();
    zlist_autofree (self->topics);
    s_parse_items (self->topics, topics);

    return self;
}


//  --------------------------------------------------------------------------
//  Destroy the tailer

void
tailer_destroy (tailer_t **self_p)
{
    assert (self_p);
    if (*self_p) {
        tailer_t *self = *self_p;
        zlist_destroy (&self->topics);
        zlist_destroy (&self->endpoints);
        zcert_destroy (&self->clientcert);
        zcert_destroy (&self->servercert);
        zsock_destroy (&self->sock);
        free (self);
        *self_p = NULL;
    }
}


//  --------------------------------------------------------------------------
//  Connect to endpoints

int
tailer_connect (tailer_t *self)
{
    const char *server_key = zcert_public_txt (self->servercert);
    zcert_apply (self->clientcert, self->sock);
    zsock_set_curve_serverkey (self->sock, server_key);

    char *topic = (char *) zlist_first (self->topics);
    while (topic) {
        switch (self->socktype) {
            case ZMQ_SUB :
                zsock_set_subscribe (self->sock, topic);
            case ZMQ_DISH :
                zsock_join (self->sock, topic);
        } 
        topic = (char *) zlist_next (self->topics);
    }

    bool error = false;
    char *endpoint = (char *) zlist_first (self->endpoints);
    while (endpoint) {
        int rc = zsock_connect (self->sock, "%s", endpoint);
        if (rc)
            error = true;
        endpoint = (char *) zlist_next (self->endpoints);
    }

    int result = 0;
    if (error)
        result = -1;

    return result;
}


//  --------------------------------------------------------------------------
//  Return the next log line

const char *
tailer_next (tailer_t *self)
{
    zmsg_t *msg = zmsg_recv (self->sock);
    const char *line = zmsg_popstr (msg);
    zmsg_destroy (&msg);
    return line;
}


//  --------------------------------------------------------------------------
//  Print properties of the tailer object.

void
tailer_print (tailer_t *self)
{
    printf ("----- tailer -----\n");

    char *endpoint = (char *) zlist_first (self->endpoints);
    while (endpoint) {
        printf ("endpoint: '%s'\n", endpoint);
        endpoint = (char *) zlist_next (self->endpoints);
    }

    char *topic = (char *) zlist_first (self->topics);
    while (topic) {
        printf ("topic: '%s'\n", topic);
        topic = (char *) zlist_next (self->topics);
    }

    printf ("------------------\n");
}


//  --------------------------------------------------------------------------
//  Self test of this class.

void
tailer_test (bool verbose)
{
    printf (" * tailer: \n");
    //  @selftest

    zactor_t *auth = zactor_new (zauth, NULL);
    assert (auth);

    zsock_t *server = zsock_new (ZMQ_PUB);
    assert (server);
    zsock_set_zap_domain (server, "global");

    assert (zsys_file_exists ("./src/example_certs/example_server"));

    zcert_t *server_cert = zcert_load ("./src/example_certs/example_server");
    assert (server_cert);

    zcert_t *client_cert = zcert_load ("./src/example_certs/example_client");
    assert (client_cert);

    zcert_apply (server_cert, server);
    zsock_set_curve_server (server, 1);

	zstr_sendx (auth, "CURVE", "./src/example_certs", NULL);
    zsock_wait (auth);
   
	zsock_bind (server, "inproc://test1");

    tailer_t *self = tailer_new (
            ZMQ_SUB,
            "inproc://test1",
            "topic1",
            "./src/example_certs/example_server",
            "./src//example_certs/example_client");

    assert (self);
    tailer_print (self);
    
    int rc = tailer_connect (self);
    assert (!rc);

    zstr_send (server, "topic1:Hello Log");

    const char *log = tailer_next (self);
    assert (streq ("topic1:Hello Log", log));
    
    tailer_destroy (&self);
    zcert_destroy (&server_cert);
    zcert_destroy (&client_cert);
    zsock_destroy (&server);
    zactor_destroy (&auth);
    //  @end
    
	printf ("OK\n");
}
