#!/usr/bin/env bash

if [ $BUILD_TYPE == "default" ]; then
    #   libsodium
    git clone git://github.com/jedisct1/libsodium.git
    cd libsodium
	./autogen.sh
	./configure
	make
	sudo make install
	sudo ldconfig 
	cd ..

    #   libzmq
    git clone git://github.com/zeromq/libzmq.git
    cd libzmq
	./autogen.sh
	./configure
	make
	sudo make install
	sudo ldconfig
	cd ..

    git clone git://github.com/zeromq/czmq.git
    cd czmq
	./autogen.sh
	./configure
	make
	sudo make install
	sudo ldconfig
	cd ..

	# libfastjson
    wget https://github.com/rsyslog/libfastjson/archive/v0.99.4.tar.gz
	tar zxvf v0.99.4.tar.gz
    cd libfastjson-0.99.4
    ./autogen.sh
    ./configure
    make
    sudo make install
    sudo ldconfig
    cd ..

    # ibestr
    wget http://libestr.adiscon.com/files/download/libestr-0.1.10.tar.gz
    tar zxvf libestr-0.1.10.tar.gz
    cd libestr-0.1.10 
    ./configure
    make
    sudo make install
    sudo ldconfig
    cd ..

	# liblognorm
    wget http://www.liblognorm.com/files/download/liblognorm-2.0.2.tar.gz
	tar zxvf liblognorm-2.0.2.tar.gz
	cd liblognorm-2.0.2 
	./configure
	make
	sudo make install
    sudo ldconfig
	cd ..

    #   Build and check this project
    ./autogen.sh
	./configure --with-docs=no
	make check
else
    cd ./builds/${BUILD_TYPE} && ./ci_build.sh
fi
