# tailz [![Build Status](https://travis-ci.org/taotetek/tailz.svg?branch=master)](https://travis-ci.org/taotetek/tailz)

![logtailz](https://i.imgur.com/DFZxsBi.png)

## Problem Statement
I wrote [logtalez](https://github.com/digitalocean/logtalez), a golang client for streaming logs from rsyslog servers. It works well, but there is some unwanted overhead in golang's cgo calls. 

## Solution
tailz - a pure C implementation of logtailez

## Installation
### Dependencies
#### [libsodium](https://github.com/jedisct1/libsodium)
Version: 1.0.2 (or newer)

Sodium is a "new, easy-to-use software library for encryption, decryption, signatures, password hashing and more".  ZeroMQ uses sodium for the basis of the CurveZMQ security protocol.

```
git clone git@github.com:jedisct1/libsodium.git
cd libsodium
./autogen.sh; ./configure; make; make check
sudo make install
sudo ldconfig
```

#### [ZeroMQ](http://zeromq.org/) 
Version: commit 6b4d9bca0c31fc8131749396fd996d17761c999f or newer

ZeroMQ is an embeddable [ZMTP](http://rfc.zeromq.org/spec:23) protocol library.

```
git clone git@github.com:zeromq/libzmq.git
cd libzmq
./autogen.sh; ./configure --with-libsodium; make; make check
sudo make install
sudo ldconfig
```

#### [CZMQ](http://czmq.zeromq.org/)
Version: commit 7997d86bcac7a916535338e71f2d826a9913df28 or newer

CZMQ is a high-level C binding for ZeroMQ.  It provides an API for various services on top of ZeroMQ such as authentication, actors, service discovery, etc.

```
git clone git@github.com:zeromq/czmq.git
cd czmq
./autogen.sh; ./configure; make; make check
sudo make install
sudo ldconfig
```

## License

This project uses the MPL v2 license, see LICENSE

